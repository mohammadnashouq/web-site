#!venv/bin/python
import os
from flask import Flask, url_for, redirect, render_template, request, abort
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user
from flask_security.utils import encrypt_password
import flask_admin
from flask_admin.contrib import sqla
from flask_admin import helpers as admin_helpers
from flask_admin import BaseView, expose
from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin
import flask_excel as excel


# Create Flask application

app = Flask(__name__)
app.config.from_pyfile('config.py')
excel.init_excel(app)
db = SQLAlchemy(app)


# Define models

roles_users = db.Table(
    'roles_users',
    
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name

class User(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean)
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return self.email
        
class Form(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(255))
    comment = db.Column(db.String(255))
    department = db.Column(db.Integer,db.ForeignKey('department.id')) 
     
    column_1 = db.Column(db.String(255))
    column_2 = db.Column(db.String(255))
    column_3 = db.Column(db.String(255))
    
   

    def __str__(self):
        return self.type_of_form

client_weekends = db.Table(
        'client_weekends',
        
        db.Column('client_id',db.Integer(), db.ForeignKey('client.id')),
        db.Column('weekend_id',db.Integer(), db.ForeignKey('weekend.id'))
)

class Weekend(db.Model, RoleMixin):

    __tablename__ =  'weekend'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

    def __str__(self):
        return self.name

class Client(db.Model, UserMixin):

    __tablename__ =  'client'
    
    id = db.Column(db.Integer, primary_key=True)
    email =  db.Column(db.String(255))
    name = db.Column(db.String(255), unique = True)
    branch_name = db.Column(db.String(255))
    telephone = db.Column(db.String(20))
    working_hours = db.Column(db.String(255)) 
    weekends = db.relationship('Weekend', secondary=client_weekends,backref=db.backref('Client Name', lazy='dynamic'))
    ramadan = db.Column(db.Boolean)
    type = db.Column(db.String(20)) # we can make new table called types
    area = db.Column(db.String(255)) 
    block = db.Column(db.String(255))
    alternative_names = db.Column(db.String(255))
    address =  db.Column(db.String(255))
    building_name = db.Column(db.String(255))
    users= db.relationship('End_user', backref='Client Name')
    greetings= db.relationship('Greeting', backref='Client Name')
    departments= db.relationship('Department',   backref='Client Name')
    comment = db.Column(db.String(255))
    google_location = db.Column(db.String(255))
    column_1 = db.Column(db.String(255))
    column_2 = db.Column(db.String(255))
    column_3 = db.Column(db.String(255))

    def __str__(self):
        return self.name

class End_user(db.Model, UserMixin):

    __tablename__ =  'End_user'
    
    id = db.Column(db.Integer, primary_key=True)
    mobile = db.Column(db.String(255))
    nick_name = db.Column(db.String(255))
    name = db.Column(db.String(255))
    address = db.Column(db.String(255))
    file_number = db.Column(db.Integer)
    client = db.Column(db.Integer,db.ForeignKey('client.id')) 
    column_1 = db.Column(db.String(255))
    column_2 = db.Column(db.String(255))
    column_3 = db.Column(db.String(255))
    
    def __str__(self):
        return self.name
    
class Greeting(db.Model, UserMixin):

    __tablename__ =  'Greeting'
    
    id = db.Column(db.Integer, primary_key=True)
    greeting = db.Column(db.String(255))
    replying = db.Column(db.String(255))    
    client = db.Column(db.Integer,db.ForeignKey('client.id')) 
    column_1 = db.Column(db.String(255))
    column_2 = db.Column(db.String(255))
    column_3 = db.Column(db.String(255))

    def __str__(self):
        return self.greeting
       
class Department(db.Model, UserMixin):

    __tablename__ =  'department'
    
    id = db.Column(db.Integer, primary_key=True)
    name =  db.Column(db.String(255))
    client = db.Column(db.Integer,db.ForeignKey('client.id')) 
    questitons= db.relationship('Common_Questions',   backref='departments')
    Promossions= db.relationship('News_Promossions',   backref='departments')
    forms= db.relationship('Form',   backref='departments')
    column_1 = db.Column(db.String(255))
    column_2 = db.Column(db.String(255))
    column_3 = db.Column(db.String(255))
   
    def __str__(self):
        return self.name

class Common_Questions(db.Model, UserMixin):

    __tablename__ =  'common_questions'
    
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(255))
    answer = db.Column(db.String(255))
    department_id = db.Column(db.Integer,db.ForeignKey('department.id'))
    column_1 = db.Column(db.String(255))
    column_2 = db.Column(db.String(255))
    column_3 = db.Column(db.String(255))
    
    def __str__(self):
        return self.question

class News_Promossions(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    news = db.Column(db.String(255))
    expiry_date = db.Column(db.DateTime)
    department_id = db.Column(db.Integer,db.ForeignKey('department.id'))
    column_1 = db.Column(db.String(255))
    column_2 = db.Column(db.String(255))
    column_3 = db.Column(db.String(255))
    
    def __str__(self):
        return self.news

# Setup Flask-Security

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

# Create customized model view class
class MyModelView(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('framework_admin_user'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))
                
    edit_modal = True
    create_modal = True    
    can_export = True
    can_view_details = True
    details_modal = True
    
    
class MyModelView2(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('framework_admin_user'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))
                
    create_template = '/admin/model/microblog_create.html'
    edit_template = '/admin/model/microblog_edit_user.html'
    edit_modal = True
    create_modal = True    
    can_export = True
    can_view_details = True
    details_modal = True
    
class MyModelView3(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('framework_admin_user'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))
                
    create_template = '/admin/model/microblog_create_client.html'     
    can_edit = True
    edit_modal = True
    create_modal = True    
    can_export = True
    can_view_details = True
    details_modal = True
    
class AnalyticsView1(BaseView):

    @expose('/')
    def index(self):
        return self.render('/admin/user.html')
        
class MyModelView1(AnalyticsView1):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('framework_admin_user'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))

    can_edit = True
    edit_modal = True
    create_modal = True    
    can_export = True
    can_view_details = True
    details_modal = True
    
class AnalyticsView(BaseView):

    def __init__(self,report,**kwargs):
    
        super().__init__( **kwargs)
        self._report = report
        
    @expose('/')
    def index(self):
    
        if self._report == 'question_report':
        
            query_res, count = self.question_report()
            return self.render('/admin/questions_report.html',res = query_res,counts = count)
            
        elif self._report == 'users_profiles':
        
            query_res, count = self.users_profiles()
            return self.render('/admin/users_profiles.html',res = query_res,counts = count)
            
        elif self._report ==  'new_and_promossions':
        
            query_res, count = self.new_and_promossions()
            return self.render('/admin/promossions_report.html',res = query_res,counts = count)
            
        elif self._report ==  'general_info':
        
            query_res, count = self.general_info()
            return self.render('/admin/general_info.html',res = query_res,counts = count)
            
        elif self._report ==  'greeting_screens':
        
            query_res, count = self.greeting_screen()
            return self.render('/admin/greeting_screen.html',res = query_res,counts = count)
            
        elif self._report ==  'forms':
        
            query_res, count = self.forms()
            return self.render('/admin/forms.html',res = query_res,counts = count)
            
            
    def forms(self):
    
        question_dept_client_list = db.session.query(Client, Department,Form).join(Department,Department.client  == Client.id).join(Form,Form.department == Department.id)
        count = question_dept_client_list.count()
        
        return question_dept_client_list,count  
        
    def greeting_screen(self):
    
        question_dept_client_list = db.session.query(Client, Greeting).join(Greeting)
        count = question_dept_client_list.count()
        
        return question_dept_client_list,count  
        
    def new_and_promossions(self):
    
        question_dept_client_list = db.session.query(Client, Department,News_Promossions).join(Department,Department.client  == Client.id).join(News_Promossions,News_Promossions.department_id == Department.id)
        count = question_dept_client_list.count()
        
        return question_dept_client_list,count
        
    def question_report(self):
    
        question_dept_client_list = db.session.query(Client, Department, Common_Questions).join(Department,Department.client == Client.id).join(Common_Questions,Common_Questions.department_id == Department.id)
        count = question_dept_client_list.count()
        
        return question_dept_client_list,count
        
    def users_profiles(self):
    
        question_dept_client_list = db.session.query(Client, End_user).join(End_user)
        count = question_dept_client_list.count()
        
        return question_dept_client_list,count
        
    def general_info(self):
    
        question_dept_client_list = db.session.query(client_weekends,Client,Weekend).join(Client).join(Weekend)
        count = question_dept_client_list.count()
        
        return question_dept_client_list,count

class UserModelView(AnalyticsView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('user'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))
                
    can_edit = False
    create_modal = False
    edit_modal = True
    create_modal = True    
    can_export = True
    can_view_details = True
    details_modal = True
    
# Flask views

class MyView(BaseView):

    def __init__(self, *args, **kwargs):
    
        self._default_view = True
        super(MyView, self).__init__(*args, **kwargs)
        self.admin = Admin()

@app.route("/admin/questions_report/export/csv/", methods=['GET'])
def get_questions_report():

    data =  db.session.query(Client, Department, Common_Questions).join(Department,Department.client == Client.id).join(Common_Questions,Common_Questions.department_id == Department.id)
    if not data == None:
    
        client_name  = []
        client_id = []
        department_name = []
        department_id =[]
        common_questions_question =[] 
        common_questions_answer = []
        common_questions_column_1 = []
        common_questions_column_2 = []
        common_questions_column_3 = []
        
        for item in data:
        
            client_name.append(item[0].name)
            client_id.append(item[0].id)
            department_name.append(item[1].name)
            department_id.append(item[1].id)
            common_questions_question.append(item[2].question)
            common_questions_answer.append(item[2].answer)
            common_questions_column_1.append(item[2].column_1)
            common_questions_column_2.append(item[2].column_2)
            common_questions_column_3.append(item[2].column_3)
            
        dic = {'Client_Name' : client_name, 'Client_id':client_id , 'Department_name': department_name , 'Department_id' : department_id , 'Question': common_questions_question , 'Answer': common_questions_answer ,'Column_1': common_questions_column_1, 'Column_2': common_questions_column_1 ,'Column_3': common_questions_column_1 }
        
    return excel.make_response_from_dict(dic,'xlsx',file_name="questions_report")
                                
@app.route("/admin/promossions_report/export/csv/", methods=['GET'])
def get_promossions_report():
        
    data =  db.session.query(Client, Department,News_Promossions).join(Department,Department.client  == Client.id).join(News_Promossions,News_Promossions.department_id == Department.id)
    column_names = ['client Name', 'Client Number','Department Name','Department Number','News','Expiry Date','Column 1','Column 2','Column 3']
  
    if not data == None:
  
        client_name  = []
        client_id = []
        department_name = []
        department_id =[]
        common_questions_question =[] 
        common_questions_answer = []
        common_questions_column_1 = []
        common_questions_column_2 = []
        common_questions_column_3 = []
        
        for item in data:
            client_name.append(item[0].name)
            client_id.append(item[0].id)
            department_name.append(item[1].name)
            department_id.append(item[1].id)
            common_questions_question.append(item[2].news)
            common_questions_answer.append(item[2].expiry_date)
            common_questions_column_1.append(item[2].column_1)
            common_questions_column_2.append(item[2].column_2)
            common_questions_column_3.append(item[2].column_3)
            
        dic = {'Client_Name' : client_name, 'Client_id':client_id , 'Department_name': department_name , 'Department_id' : department_id , 'News': common_questions_question , 'Expiry_date': common_questions_answer ,'Column_1': common_questions_column_1, 'Column_2': common_questions_column_1 ,'Column_3': common_questions_column_1 }
        
    return excel.make_response_from_dict(dic,'xlsx',file_name="promossions_report")
   
@app.route("/admin/greeting_screen/export/csv/", methods=['GET'])
def get_greeting_screen():
        
    data = db.session.query(Client, Greeting).join(Greeting)
    column_names = ['client Name', 'Client Number','Greeting','Replying','Column 1','Column 2','Column 3']
  
    if not data == None:
  
        client_name  = []
        client_id = []
        department_name = []
        department_id =[]
        common_questions_question =[] 
        common_questions_answer = []
        common_questions_column_1 = []
        common_questions_column_2 = []
        common_questions_column_3 = []
        
        for item in data:
        
            client_name.append(item[0].name)
            client_id.append(item[0].id)
            department_name.append(item[1].greeting)
            department_id.append(item[1].replying)
            common_questions_column_1.append(item[1].column_1)
            common_questions_column_2.append(item[1].column_2)
            common_questions_column_3.append(item[1].column_3)
            
        dic = {'Client_Name' : client_name, 'Client_id':client_id , 'Greeting': department_name , 'Replying' : department_id ,'Column_1': common_questions_column_1, 'Column_2': common_questions_column_1 ,'Column_3': common_questions_column_1 }
        
    return excel.make_response_from_dict(dic,'xlsx',file_name="greeting_screen")
               
@app.route("/admin/forms/export/csv/", methods=['GET'])
def get_forms():

    data =   db.session.query(Client, Department,Form).join(Department,Department.client  == Client.id).join(Form,Form.department == Department.id)
    column_names = ['client Name', 'Client Number','Department Name','Department Number','Type Of Form','Comment','Column 1','Column 2','Column 3']

    if not data == None:
    
        client_name  = []
        client_id = []
        department_name = []
        department_id =[]
        common_questions_question =[] 
        common_questions_answer = []
        common_questions_column_1 = []
        common_questions_column_2 = []
        common_questions_column_3 = []
        
        for item in data:
        
            client_name.append(item[0].name)
            client_id.append(item[0].id)
            department_name.append(item[1].name)
            department_id.append(item[1].id)
            common_questions_question.append(item[2].type)
            common_questions_answer.append(item[2].comment)
            common_questions_column_1.append(item[2].column_1)
            common_questions_column_2.append(item[2].column_2)
            common_questions_column_3.append(item[2].column_3)
            
        dic = {'Client_Name' : client_name, 'Client_id':client_id , 'Department_Name': department_name , 'Department_Number' : department_id ,'Type_Of_Form' : common_questions_question,'Comment':common_questions_answer,'Column_1': common_questions_column_1, 'Column_2': common_questions_column_1 ,'Column_3': common_questions_column_1 }
        
    return excel.make_response_from_dict(dic,'xlsx',file_name="forms")
                 
@app.route("/admin/general_info/export/csv/", methods=['GET'])
def general_info():
        
    data =   db.session.query(client_weekends,Client,Weekend).join(Client).join(Weekend)
    column_names = ['client Name', 'Client Number','Branch Name','Telephone','Working Hours text','Weekends','Ramadan','Type','Area','Block','Alternative Names','Google Map Location','Address','Building Name','Comment','Column 1','Column 2','Column 3']
    
    if not data == None:
    
        client_name  = []
        client_id = []
        branch = []
        telephone = []
        working_hours = []
        weekends = []
        ramadan = []
        type =[]
        area = []
        block = []
        alternative_names = []
        google_location = []
        address = []
        building_name = []
        comment = []
        common_questions_column_1 = []
        common_questions_column_2 = []
        common_questions_column_3 = []
        
        for item in data:
        
            client_name.append(item[2].name)
            client_id.append(item[2].id)
            branch.append(item[2].branch_name)
            telephone.append(item[2].telephone)
            working_hours.append(item[2].working_hours)
            weekends.append(item[3].name)
            ramadan.append(item[2].ramadan)
            type.append(item[2].type)
            area.append(item[2].area)
            block.append(item[2].block)
            alternative_names.append(item[2].alternative_names)
            google_location.append(item[2].google_location)
            address.append(item[2].address)
            building_name.append(item[2].building_name)
            comment.append(item[2].comment)
            common_questions_column_1.append(item[2].column_1)
            common_questions_column_2.append(item[2].column_2)
            common_questions_column_3.append(item[2].column_3)
            
        dic = {'Client_Name' : client_name, 'Client_id':client_id , 'Branch_name': branch , 'Telephone' : telephone ,'Working_Hours' : working_hours,'Weekends':weekends,'Ramadan':ramadan,'Type':type,'Area':area,'Block':block,'Alternative_Names':alternative_names,'Google_Location':google_location,'Address':address,'Building_Name':building_name,'Comment':comment,'Column_1': common_questions_column_1, 'Column_2': common_questions_column_1 ,'Column_3': common_questions_column_1 }
        
    return excel.make_response_from_dict(dic,'xlsx',file_name="general_info")
             
             
@app.route("/admin/users_profiles/export/csv/", methods=['GET'])
def get_users_profiles():

    data =  db.session.query(Client, End_user).join(End_user)
    column_names = ['client Name', 'Client Number','Mobile','Name','Nick name','File Number','Address','Column 1','Column 2','Column 3']

    if not data == None:

        client_name  = []
        client_id = []
        mobile = []
        name = []
        nick_name = [] 
        file_number = []
        address = []
        common_questions_column_1 = []
        common_questions_column_2 = []
        common_questions_column_3 = []
        
        for item in data:
            client_name.append(item[0].name)
            client_id.append(item[0].id)
            mobile.append(item[1].mobile)
            name.append(item[1].name)
            nick_name.append(item[1].nick_name)
            file_number.append(item[1].file_number)
            address.append(item[1].address)
            common_questions_column_1.append(item[1].column_1)
            common_questions_column_2.append(item[1].column_2)
            common_questions_column_3.append(item[1].column_3)
            
        dic = {'Client_Name' : client_name, 'Client_id':client_id , 'Mobile': mobile , 'User_name' : name ,'Nick_Name' : nick_name,'File_Number':file_number,'Address' : address,'Column_1': common_questions_column_1, 'Column_2': common_questions_column_1 ,'Column_3': common_questions_column_1 }
        
    return excel.make_response_from_dict(dic,'xlsx',file_name="users_profiles")

@app.route('/')
def index():

    return MyView().render('index.html')
# Create admin

admin = flask_admin.Admin(
    app,
    'My Dashboard',
    base_template='my_master.html',
    template_mode='bootstrap3',
)
  

# Add model views

admin.add_view(MyModelView(Role,db.session, category="tables"))
admin.add_view(MyModelView2(User,db.session, category="tables"))  
admin.add_view(MyModelView3(Client,db.session, category="tables"))
admin.add_view(MyModelView(Weekend,db.session, category="tables"))  
admin.add_view(MyModelView(End_user,db.session, category="tables")) 
admin.add_view(MyModelView(Department,db.session, category="tables"))  
admin.add_view(MyModelView(Greeting,db.session, category="tables")) 
admin.add_view(MyModelView(Common_Questions,db.session, category="tables"))
admin.add_view(MyModelView(News_Promossions,db.session, category="tables"))  
admin.add_view(MyModelView(Form,db.session, category="tables") ) 
    
# add reports

admin.add_view(UserModelView(report = 'general_info',name='General Info', endpoint='General Info' ,category="reports"))
admin.add_view(UserModelView(report = 'users_profiles',name='Users Profiles', endpoint='Users Profiles', category="reports" ))
admin.add_view(UserModelView(report = 'question_report',name='Questions Report', endpoint='Questions Report' , category="reports"))
admin.add_view(UserModelView(report = 'new_and_promossions',name='New And Promossions', endpoint='New And Promossions' ,category="reports"))
admin.add_view(UserModelView(report = 'greeting_screens',name='Greeting Screens', endpoint='Greeting Screens' ,category="reports"))
admin.add_view(UserModelView(report = 'forms',name='Forms', endpoint='Forms',category="reports"))  


# flask-security views.
@security.context_processor
def security_context_processor():

    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )

if __name__ == '__main__':
    excel.init_excel(app)
    app.run(debug=True)